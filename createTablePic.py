#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sqlite3
import configparser
import os

import dateUtils
import constants

def checkNone(val):
    if val == None:
        return ""
    return val

def getBoxColor(val, high, low):
    if val > high: 
        return "red"
    elif val < low: 
        return "blue"
    return "black"

def getColor(defaultcolor, value, defaultvalue = 60 ):
    if value >= defaultvalue:
        return 'red'
    return defaultcolor

def modifyTime(value):
    if value > 59 and value < (24*60):
       return str(int(value / 60))+'h'
    elif value >= (24*60):
       return str(int(value / 60 / 24))+'d'
    return str(value)+'min'

def interpolate_BOILER_value(value):
    mydict = {}
    import csv
    with open(config.get('main', 'PATH_BOILER_CALIBRATE_FILE')) as csvfile:
        lines = csv.reader(csvfile, delimiter='|')
        count = 0
        for line in lines:
            if count == 0:
                count += 1
                continue
            mydict[float(line[0])] = [float(line[1]), line[2], line[3], float(line[4])]
            count += 1
    count = 0
    sortedkeys = sorted(mydict)
    for key in sortedkeys:
        if key > value:
            break
        count += 1
    min = mydict[sortedkeys[count-1]][0]
    max = mydict[sortedkeys[count]][0]

    slope = (max-min)/(sortedkeys[count]-sortedkeys[count-1])
    interpolatedval = slope * (value - sortedkeys[count-1]) + min
    return round(interpolatedval, 1)

os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
config = configparser.ConfigParser()
config.read('config.ini')
config.get('main', 'PATH_GRAPH_OUTPUT')
conn = sqlite3.connect(config.get('main', 'DB_PATH_NAME'))

WZ_id = 6
SZ_id = 8
WE_id = 9
KU_id = 4
SA_id = 2
AU_id = 5
GA_id = 3
BOILER_id = 12
HUHN_id = 11

id_list = {SA_id, GA_id, KU_id, AU_id, WZ_id, SZ_id, WE_id, BOILER_id, HUHN_id}
values_temp = {}

for i_id in id_list:    
    sql_id = constants.select_id_last_data_json.format(i_id, "value1, value2")
    sql_desc = constants.select_id_desc_temp_graph.format(i_id)
    cur_id = conn.execute(sql_id)
    cur_desc = conn.execute(sql_desc)
    rt_desc = cur_desc.fetchone()
    for row in cur_id:
        values_temp[i_id] = {}    
        values_temp[i_id]['val1'] = row[1]/float(rt_desc[1]);
        if not row[2] == None:
            values_temp[i_id]['val2'] = row[2]/float(rt_desc[1]);
        else:
            values_temp[i_id]['val2'] = None
        values_temp[i_id]['label'] = rt_desc[0]
        values_temp[i_id]['lastdate'] = row[0]


min_max_temps={}

for i_id in id_list:
    sql_min = constants.select_min_temp.format(i_id,dateUtils.get_UTC_one_year_ago())
    sql_max = constants.select_max_temp.format(i_id,dateUtils.get_UTC_one_year_ago())

    cur_min=conn.execute(sql_min)
    cur_max=conn.execute(sql_max)

    min_temp=cur_min.fetchone()
    max_temp=cur_max.fetchone()

    min_max_temps[i_id]={}
    min_max_temps[i_id]["minDate"] = min_temp[0]
    min_max_temps[i_id]["minTemp"] = min_temp[1]/100
    min_max_temps[i_id]["maxDate"] = max_temp[0]
    min_max_temps[i_id]["maxTemp"] = max_temp[1]/100


my_font_size = 12

fig = plt.figure(figsize=(12.8, 10.24), dpi=300, facecolor='black')
ax = fig.add_subplot(111, facecolor='black')

yposition = 0.05
yspace = 0.05
xpositioncol1=0.25
xpositioncol2=0.5
xpositioncol3 = 0.8
xpositioncol4 = 0.9
alphabluebox = 0.3

from matplotlib.lines import Line2D
l = Line2D([0.00, 0.97], [yposition+yspace*6+0.03, yposition+yspace*6+0.03], color='white', alpha=0.2)                                    
ax.add_line(l)
l = Line2D([0.30, 0.30], [yposition+yspace*6+0.05, 0.97], color='white', alpha=0.2)                                    
ax.add_line(l)
l = Line2D([0.67, 0.67], [yposition+yspace*6+0.05, 0.97], color='white', alpha=0.2)                                    
ax.add_line(l)

# Verbrauchszeug
import consumption

strom_consumptions=[]
wasser_consumptions=[]
months=[]

for i in dateUtils.get_last_12_months():
    strom_consumptions.append(round(consumption.createConsumptionForMonth("strom",i.year,i.month),1))
    wasser_consumptions.append(round(consumption.createConsumptionForMonth("wasser",i.year,i.month),1))
    months.append(i.strftime("%b"))

plt.subplots_adjust(wspace=0.5,)


strom=fig.add_subplot(7,3,10)
strom.patch.set_alpha(0)
strom.bar(range(len(strom_consumptions)),strom_consumptions)
strom.yaxis.label.set_color('red')
strom.tick_params(axis='x', colors='red')
strom.tick_params(axis='y', colors='red')
strom.spines['left'].set_color('red')
strom.spines['bottom'].set_color('red')
strom.set_ylabel("Verbrauch [kWH]")
strom.set_xticks(range(len(months)))
strom.set_xticklabels(months)

for i in strom.get_xticklabels():
    i.set_rotation(90)

wasser=fig.add_subplot(7,3,11)
wasser.patch.set_alpha(0)
wasser.bar(range(len(wasser_consumptions)),wasser_consumptions)
wasser.yaxis.label.set_color('red')
wasser.tick_params(axis='x', colors='red')
wasser.tick_params(axis='y', colors='red')
wasser.spines['left'].set_color('red')
wasser.spines['bottom'].set_color('red')
wasser.set_ylabel("Verbrauch [l]")
wasser.set_xticks(range(len(months)))
wasser.set_xticklabels(months)

for i in wasser.get_xticklabels():
    i.set_rotation(90)

ax.text(0.00, 0.95, '{0}'.format('Strom'), fontsize=17, color='white', alpha=0.35)
ax.text(0.33, 0.95, '{0}'.format('Wasser'), fontsize=17, color='white', alpha=0.35) 
ax.text(0.70, 0.95, '{0}'.format('Spezial'), fontsize=17, color='white', alpha=0.35) 
ax.text(0.70, 0.68, '{0}'.format('HuehnerHaus'), fontsize=17, color='white', alpha=0.35) 
ax.text(0.00, 0.88, '{0}'.format('Gesamtzeitraum'), fontsize=15, color='white', alpha=0.35) 
ax.text(0.00, 0.85, 'AVG Tag: {0} kWh'.format(round(consumption.getDailyAverageConsumption("strom"),1)), fontsize=17, color='white')
ax.text(0.00, 0.78, '{0}'.format('Aktuelles Monat'), fontsize=15, color='white', alpha=0.35) 
ax.text(0.00, 0.75, 'AVG Tag: {0} kWh'.format(round(consumption.createConsumptionActualMonth("strom"),1)), fontsize=17, color='white')
ax.text(0.00, 0.73, 'letzte Erfassung am: {0}'.format(consumption.get_latest_insert_date("strom")), fontsize=11, color='white', alpha=0.35)
ax.text(0.00, 0.68, '{0}'.format('Letztes Monat'), fontsize=15, color='white', alpha=0.35) 
ax.text(0.00, 0.65, 'AVG Tag: {0} kWh'.format(round(consumption.createConsumptionLastMonth("strom"),1)), fontsize=17, color='white')

ax.text(0.33, 0.88, '{0}'.format('Gesamtzeitraum'), fontsize=15, color='white', alpha=0.35) 
ax.text(0.33, 0.85, 'AVG Tag: {0} L'.format(round(consumption.getDailyAverageConsumption("wasser"),1)), fontsize=17, color='white') 
ax.text(0.33, 0.78, '{0}'.format('Aktuelles Monat'), fontsize=15, color='white', alpha=0.35) 
ax.text(0.33, 0.75, 'AVG Tag: {0} L'.format(round(consumption.createConsumptionActualMonth("wasser"),1)), fontsize=17, color='white') 
ax.text(0.33, 0.73, 'letzte Erfassung am: {0}'.format(consumption.get_latest_insert_date("wasser")), fontsize=11, color='white', alpha=0.35)
ax.text(0.33, 0.68, '{0}'.format('Letztes Monat'), fontsize=15, color='white', alpha=0.35) 
ax.text(0.33, 0.65, 'AVG Tag: {0} L'.format(round(consumption.createConsumptionLastMonth("wasser"),1)), fontsize=17, color='white') 
ax.text(0.70, 0.88, '{0}'.format('Boiler'), fontsize=15, color='white', alpha=0.35) 
ax.text(0.70, 0.85, '{0}/{1} C'.format(interpolate_BOILER_value(values_temp[BOILER_id]['val1']), values_temp[BOILER_id]['val1']), fontsize=17, color='white') 
ax.text(0.70, 0.65, '{0} C'.format(values_temp[HUHN_id]['val1']), fontsize=17, color='white') 

ax.text(0.7, 0.80, '{0}°C am {1}'.format(min_max_temps[BOILER_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[BOILER_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(0.7, 0.77, '{0}°C am {1}'.format(min_max_temps[BOILER_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[BOILER_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ

ax.text(0.7, 0.60, '{0}°C am {1}'.format(min_max_temps[HUHN_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[HUHN_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(0.7, 0.57, '{0}°C am {1}'.format(min_max_temps[HUHN_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[HUHN_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ


ax.text(0, yposition+yspace*6, '{0}'.format(values_temp[SA_id]['label']), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(0, yposition+yspace*5, '{0}'.format(values_temp[GA_id]['label']), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(0, yposition+yspace*4, '{0}'.format(values_temp[KU_id]['label']), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(0, yposition+yspace*3, '{0}'.format(values_temp[AU_id]['label']), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(0, yposition+yspace*2, '{0}'.format(values_temp[WZ_id]['label']), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(0, yposition+yspace*1, '{0}'.format(values_temp[SZ_id]['label']), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(0, yposition+yspace*0, '{0}'.format(values_temp[WE_id]['label']), fontsize=my_font_size, color='white', alpha=0.455) # WZ


ax.text(xpositioncol1, yposition+yspace*6, '{0}°C am {1}'.format(min_max_temps[SA_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[SA_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol1, yposition+yspace*5, '{0}°C am {1}'.format(min_max_temps[GA_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[GA_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol1, yposition+yspace*4, '{0}°C am {1}'.format(min_max_temps[KU_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[KU_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol1, yposition+yspace*3, '{0}°C am {1}'.format(min_max_temps[AU_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[AU_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol1, yposition+yspace*2, '{0}°C am {1}'.format(min_max_temps[WZ_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[WZ_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol1, yposition+yspace*1, '{0}°C am {1}'.format(min_max_temps[SZ_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[SZ_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol1, yposition+yspace*0, '{0}°C am {1}'.format(min_max_temps[WE_id]['minTemp'],dateUtils.get_UTC_as_Date(min_max_temps[WE_id]['minDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ


ax.text(xpositioncol2, yposition+yspace*6, '{0}°C am {1}'.format(min_max_temps[SA_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[SA_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol2, yposition+yspace*5, '{0}°C am {1}'.format(min_max_temps[GA_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[GA_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol2, yposition+yspace*4, '{0}°C am {1}'.format(min_max_temps[KU_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[KU_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol2, yposition+yspace*3, '{0}°C am {1}'.format(min_max_temps[AU_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[AU_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol2, yposition+yspace*2, '{0}°C am {1}'.format(min_max_temps[WZ_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[WZ_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol2, yposition+yspace*1, '{0}°C am {1}'.format(min_max_temps[SZ_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[SZ_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ
ax.text(xpositioncol2, yposition+yspace*0, '{0}°C am {1}'.format(min_max_temps[WE_id]['maxTemp'],dateUtils.get_UTC_as_Date(min_max_temps[WE_id]['maxDate']).strftime("%d.%m.%Y")), fontsize=my_font_size, color='white', alpha=0.455) # WZ

ax.text(xpositioncol3, yposition+yspace*6, '{:.1f} C'.format(values_temp[SA_id]['val1']), fontsize=my_font_size, color='white', 
    bbox={'facecolor':getBoxColor(values_temp[SA_id]['val1'], 28, 5), 'alpha':alphabluebox, 'pad':8})
ax.text(xpositioncol3, yposition+yspace*5, '{:.1f} C'.format(values_temp[GA_id]['val1']), fontsize=my_font_size, color='white', 
    bbox={'facecolor':getBoxColor(values_temp[GA_id]['val1'], 28, 18), 'alpha':alphabluebox, 'pad':8}) # WZ
ax.text(xpositioncol3, yposition+yspace*4, '{:.1f} C'.format(values_temp[KU_id]['val1']), fontsize=my_font_size, color='white', 
    bbox={'facecolor':getBoxColor(values_temp[KU_id]['val1'], 28, 18), 'alpha':alphabluebox, 'pad':8}) # WZ
ax.text(xpositioncol3, yposition+yspace*3, '{:.1f} C'.format(values_temp[AU_id]['val1']), fontsize=my_font_size, color='white', 
    bbox={'facecolor':getBoxColor(values_temp[AU_id]['val1'], 28, 18), 'alpha':alphabluebox, 'pad':8}) # WZ
ax.text(xpositioncol3, yposition+yspace*2, '{:.1f} C'.format(values_temp[WZ_id]['val1']), fontsize=my_font_size, color='white', 
    bbox={'facecolor':getBoxColor(values_temp[WZ_id]['val1'], 28, 18), 'alpha':alphabluebox, 'pad':8}) # WZ
ax.text(xpositioncol3, yposition+yspace*1, '{:.1f} C'.format(values_temp[SZ_id]['val1']), fontsize=my_font_size, color='white', 
    bbox={'facecolor':getBoxColor(values_temp[SZ_id]['val1'], 28, 18), 'alpha':alphabluebox, 'pad':8}) # WZ
ax.text(xpositioncol3, yposition+yspace*0, '{:.1f} C'.format(values_temp[WE_id]['val1']), fontsize=my_font_size, color='white', 
    bbox={'facecolor':getBoxColor(values_temp[WE_id]['val1'], 28, 18), 'alpha':alphabluebox, 'pad':8}) # WZ

#ax.text(yposition+yspace*5, yposition+yspace*6, '{0}'.format(checkNone(values_temp[SA_id]['val2'])), fontsize=my_font_size, color='white') # WZ
#ax.text(yposition+yspace*5, yposition+yspace*5, '{0}'.format(checkNone(values_temp[GA_id]['val2'])), fontsize=my_font_size, color='white') # WZ
#ax.text(yposition+yspace*5, yposition+yspace*4, '{0}'.format(checkNone(values_temp[KU_id]['val2'])), fontsize=my_font_size, color='white') # WZ
#ax.text(yposition+yspace*5, yposition+yspace*3, '{0}'.format(checkNone(values_temp[AU_id]['val2'])), fontsize=my_font_size, color='white') # WZ
#ax.text(yposition+yspace*5, yposition+yspace*2, '{0}'.format(checkNone(values_temp[WZ_id]['val2'])), fontsize=my_font_size, color='white') # WZ
#ax.text(yposition+yspace*5, yposition+yspace*1, '{0}'.format(checkNone(values_temp[SZ_id]['val2'])), fontsize=my_font_size, color='white') # WZ
#ax.text(yposition+yspace*5, yposition+yspace*0, '{0}'.format(checkNone(values_temp[WE_id]['val2'])), fontsize=my_font_size, color='white') # WZ

import calendar
nowInSeconds = calendar.timegm(dateUtils.createDateTimeToday().timetuple())
ax.text(xpositioncol4, yposition+yspace*6, '{0}'.format(modifyTime((nowInSeconds - values_temp[SA_id]['lastdate']) / 60)), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(xpositioncol4, yposition+yspace*5, '{0}'.format(modifyTime((nowInSeconds - values_temp[GA_id]['lastdate']) / 60)), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(xpositioncol4, yposition+yspace*4, '{0}'.format(modifyTime((nowInSeconds - values_temp[KU_id]['lastdate']) / 60)), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(xpositioncol4, yposition+yspace*3, '{0}'.format(modifyTime((nowInSeconds - values_temp[AU_id]['lastdate']) / 60)), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(xpositioncol4, yposition+yspace*2, '{0}'.format(modifyTime((nowInSeconds - values_temp[WZ_id]['lastdate']) / 60)), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(xpositioncol4, yposition+yspace*1, '{0}'.format(modifyTime((nowInSeconds - values_temp[SZ_id]['lastdate']) / 60)), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(xpositioncol4, yposition+yspace*0, '{0}'.format(modifyTime((nowInSeconds - values_temp[WE_id]['lastdate']) / 60)), fontsize=my_font_size, color='white', alpha=0.455)
ax.text(0.90, 0.85, '{0}'.format(modifyTime((nowInSeconds - values_temp[BOILER_id]['lastdate']) / 60)), fontsize=17, color='white', alpha=0.35)
ax.text(0.90, 0.65, '{0}'.format(modifyTime((nowInSeconds - values_temp[HUHN_id]['lastdate']) / 60)), fontsize=17, color='white', alpha=0.35)


ax.text(0.63, 0, "created " + dateUtils.get_formated_str_local_time(), fontsize=10, color='white', alpha=0.25)
fig.savefig(config.get('main', 'PATH_GRAPH_OUTPUT') + os.sep + "table{0}.png".format(""), facecolor=fig.get_facecolor())

