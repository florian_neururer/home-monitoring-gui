#!/usr/bin/env python
# -*- coding: utf-8 -*- 

# CREATE ---------------------------------------------------------------------------------------------
create_id_data_sql = '''CREATE TABLE ID{0}_DATA(
  datecolumn INTEGER primary key,
  current INTEGER not null,
  value1 INTEGER not null,
  value2 INTEGER
);'''
create_room_table_sql = '''CREATE TABLE IF NOT EXISTS rooms 
(
    nr INTEGER primary key, 
    name TEXT, 
    floornr INTEGER NOT NULL
);'''
create_floor_table_sql = '''CREATE TABLE IF NOT EXISTS floors 
(
    nr INTEGER primary key, 
    name TEXT
);'''
create_status_table_sql = '''CREATE TABLE IF NOT EXISTS reed_status
(
datecolumn INTEGER PRIMARY KEY NOT NULL,
status INTEGER NOT NULL
);'''

# INSERTS ---------------------------------------------------------------------------------------------
insert_into_status = '''INSERT INTO reed_status (datecolumn, status) VALUES ({0}, '{1}');'''
insert_into_floors = '''INSERT INTO floors (nr, name) VALUES ({0}, '{1}');'''
insert_into_rooms = '''INSERT INTO rooms (nr, name, room_nr) VALUES ({0}, '{1}', {2});'''
insert_id_data_sql_1value = 'INSERT INTO ID{0}_DATA (datecolumn, current, value1) VALUES ({1},{2},{3});'
insert_id_data_sql_2value = 'INSERT INTO ID{0}_DATA (datecolumn, current, value1, value2) VALUES ({1},{2},{3},{4});'
insert_into_desc = '''INSERT INTO id_descriptions (
                        id, 
                        name, 
                        description, 
                        description_long, 
                        factor_current, 
                        factor_sensor1, 
                        factor_sensor2,
                        second_sensor, 
                        floor_nr, 
                        room_nr
                    ) 
                    values ({0}, '{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, {8}, {9});'''
insert_into_strom = '''INSERT INTO strom_verbrauch (datum, value) VALUES ({0}, {1});'''
insert_into_wasser = '''INSERT INTO wasser_verbrauch (datum, value) VALUES ({0}, {1});'''
insert_into_sprit = '''INSERT INTO sprit_data (datum, tankenr, value) VALUES ({0}, {1}, {2});'''

# SELECTS ---------------------------------------------------------------------------------------------
select_id_desc = '''SELECT 
                    name, 
                    description, 
                    description_long, 
                    factor_current, 
                    factor_sensor1, 
                    factor_sensor2, 
                    second_sensor, 
                    floor_nr, 
                    room_nr 
                    FROM id_descriptions WHERE id={0};'''
select_id_desc_temp_graph   = 'SELECT description, factor_sensor1, factor_current FROM id_descriptions WHERE id={0};'
select_id_data_json         = 'SELECT datecolumn, {1} FROM id{0}_data where datecolumn >= {2} and datecolumn <= {3} order by datecolumn;'
select_id_last_data_json    = 'SELECT datecolumn, {1} FROM id{0}_data order by datecolumn DESC LIMIT 1;'
select_id_data_graph1       = 'SELECT datecolumn, value1 FROM id{0}_data where datecolumn > {1} order by datecolumn;'
select_id_data_voltage      = 'SELECT datecolumn, current FROM id{0}_data where datecolumn > {1} order by datecolumn;'
select_id_data_graph2       = 'SELECT datecolumn, value2 FROM id{0}_data where datecolumn > {1} order by datecolumn;'
select_all_tables           = 'SELECT name FROM sqlite_master;'
select_all_id_data_sql      = 'SELECT * FROM id_descriptions order by id;'
select_secondval_id_desc_sql = 'SELECT id, second_sensor FROM id_descriptions order by id;'
select_all_floor_data_sql   = 'SELECT * FROM floors order by nr;'
select_all_room_data_sql    = 'SELECT * FROM rooms order by nr;'
select_id_data_sql          = 'SELECT datecolumn, current, value1, value2 FROM ID{0}_DATA ORDER BY datecolumn DESC LIMIT 1;'

select_act_consumption_sql  = 'SELECT value from {0}_verbrauch ORDER BY datum DESC LIMIT 1;'


select_min_temp             = 'SELECT datecolumn, MIN(value1) FROM ID{0}_DATA WHERE datecolumn>{1};'
select_max_temp             = 'SELECT datecolumn, MAX(value1) FROM ID{0}_DATA WHERE datecolumn>{1};'


# UPDATES ---------------------------------------------------------------------------------------------
update_id_desc = '''UPDATE id_descriptions SET
                    name='{0}', 
                    description='{1}', 
                    description_long='{2}',  
                    factor_current={3}, 
                    factor_sensor1={4}, 
                    factor_sensor2={5}, 
                    second_sensor={6}, 
                    floor_nr={7},  
                    room_nr={8} 
                    WHERE id={9};
                 '''
# DELETES ---------------------------------------------------------------------------------------------
delete_id_data_sql = 'DELETE FROM id_descriptions WHERE id={0};'