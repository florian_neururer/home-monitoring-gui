import sqlite3
from datetime import datetime,timedelta
import re
from time import mktime

con = sqlite3.connect("fuho.db")

c = con.cursor();

c.execute("SELECT name FROM sqlite_master WHERE type='table'")
tables = c.fetchall()

sensors=[];
rx=re.compile("ID[0-9]*_DATA")
for id in tables:
    if(rx.match(id[0])):
        sensors.append(id[0])

print(sensors)

t = datetime.now() - timedelta(days=365)
unix_secs = mktime(t.timetuple())
values=[]
for sensor in sensors:
    c.execute("SELECT datecolumn,MIN(current),MAX(current) FROM "+sensor+" WHERE datecolumn>?",(int(unix_secs),))
    values.append(c.fetchone())

print(values)
c.close()

# Verbrauchsdatenbank fehlt
# Mittel von was(tag oder Monat)
# Form der Graphen
# 90° Gedreht ?
#
# Fehlerhafte Tabellen
# Woher Daten zur Dynamischen Auswertung
