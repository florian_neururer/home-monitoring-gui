#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import calendar
import datetime as dt
import pytz
import time

'''
dateUtils.createDateTimeTomorowSeconds()*1000
UTS_begin_in2d = dateUtils.createDateTimeIn2DaysSeconds()*1000
'''

## input paramtyp is SECOND
## ----------------------------------------------------------------------------------------------
def createDateAndTimeFromSeconds(seconds):
    newdate = createDateTimeFromSeconds(seconds)
    return newdate.strftime("%Y-%m-%d %H:%M")

def createTimeFromSeconds(seconds, local=False):
    if local:
        newdate = dt.datetime.fromtimestamp(seconds)
    else:
        newdate = createDateTimeFromSeconds(seconds)
    return newdate.strftime("%H:%M")

def createDateAndTimeFromSecondsLocal(seconds):
    return convertUTCSecondstoLocalDatetime(seconds).strftime("%Y-%m-%d %H:%M")

def createDayofWeekFromSeconds(seconds):
    newdate = createDateTimeFromSeconds(seconds)
    return newdate.strftime("%a")

def createDateTimeFromSeconds(seconds):
    return dt.datetime.utcfromtimestamp(seconds)

def convertUTCSecondstoLocalseconds(second):
    return calendar.timegm(convertUTCSecondstoLocalDatetime(second).timetuple())

def convertUTCSecondstoLocalDatetime(second):
    from dateutil import tz
    return createDateTimeFromSeconds(second).replace(tzinfo=tz.tzutc()).astimezone(tz.tzlocal())
## ----------------------------------------------------------------------------------------------
## ----------------------------------------------------------------------------------------------

# param date is a STRING
def createDateTime(date):
    if len(date) > 8:
        return createDateTimeWithHOURAndMinute(date)
    
    return createDateTimeWithHOURAndMinute(date + '0000')

def createDateTimeWithHOURAndMinute(date):
    #print date
    y = int(date[0:4])
    m = int(date[4:6])
    d = int(date[6:8])
    H = int(date[8:10])
    M = int(date[10:12])
    return dt.datetime(y,m,d,H,M,0,0,pytz.UTC)

def show_today_or_yesterday_str(mydt):
    now = dt.datetime.now()
    daydiff = (now - mydt).days
    if daydiff == 0:
        return 'Heute'
    elif daydiff == 1:
        return 'Gestern'
    elif daydiff == 2:
        return 'Vor-Gestern'
    else:
        return mydt

def createSecondsSince(date):    
    givendate_dt = createDateTime(date)
    return calendar.timegm(givendate_dt.timetuple())

def createDateTimeToday():
    return dt.datetime.now(pytz.UTC)

def createDateTodaySeconds():
    return calendar.timegm(dt.date.today().timetuple())

def createDateTimeTodaySeconds():
    return calendar.timegm(dt.datetime.now(pytz.UTC).timetuple())

def createDateTimeTomorowSeconds():
    return dateInParamGivenSeconds(1)

def createDateTimeIn2DaysSeconds():
    return dateInParamGivenSeconds(2)

def dateInParamGivenSeconds(daycount):
    aft_tomorrow = dt.date.today() + dt.timedelta(days=daycount)
    return calendar.timegm(aft_tomorrow.timetuple())

def createUTCSecondsNow():
    import time
    return int(time.time())

def getPastDateAsStrWithDeltaHourFromNow(deltaInHours):
    dtpast = createDateTimeToday() - dt.timedelta(hours=deltaInHours)
    return dtpast.strftime("%Y%m%d%H%M")

def getPastDateAsStrWithDelteFromNow(deltaInDays):
    dtpast = createDateTimeToday() - dt.timedelta(days=deltaInDays)
    return dtpast.strftime("%Y%m%d%H%M")

def get_formated_str_local_time():
    tz = pytz.timezone('Europe/Berlin')
    now = dt.datetime.now(tz)
    return now.strftime("%Y-%m-%d %H:%M")

def get_UTC_one_year_ago():
    t = dt.datetime.now() - dt.timedelta(days=365)
    unix_secs = time.mktime(t.timetuple())
    return int(unix_secs)

def get_UTC_as_Date(utc):
    return dt.datetime.fromtimestamp(utc)

def get_last_12_months():
    t = dt.datetime.now() - dt.timedelta(days=365)
    months=[]
    for i in range(12):
        months.append(add_months(t,i))
    return months
    
    
def add_months(sourcedate,months):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12 )
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return dt.date(year,month,day)





#print show_today_or_yesterday_str(dt.datetime(2016,10,3,0,0,0,0))
#print createDateTimeTodaySeconds()
#print createDateTimeTomorowSeconds()
#print createDateTimeIn2DaysSeconds()