
import sqlite3
from datetime import datetime,timedelta
import re 
from time import mktime


con = sqlite3.connect("fuho_new.db")

c = con.cursor();

WZ_id = 6
SZ_id = 8
WE_id = 9
KU_id = 4
SA_id = 2
AU_id = 5
GA_id = 3
BOILER_id = 12
HUHN_id = 11

id_list = {SA_id, GA_id, KU_id, AU_id, WZ_id, SZ_id, WE_id, BOILER_id, HUHN_id}
t = datetime.now() - timedelta(days=365)
unix_secs = mktime(t.timetuple())
values=[]

for id in id_list:
    c.execute("SELECT datecolumn,MIN(value1) FROM ID"+str(id)+"_DATA WHERE datecolumn>?",(int(unix_secs),))
    values.append(c.fetchone())

print(values)


c.close()