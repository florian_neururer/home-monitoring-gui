#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import dateUtils
import calendar

import os
import sqlite3
import configparser
import datetime as dt

def get_latest_insert_date(typ):
    conn = createConn()
    sql = "SELECT datum FROM {0}_verbrauch ORDER BY datum DESC LIMIT 1;".format(typ)
    cur = conn.execute(sql)
    res = cur.fetchone()
    last_date = dateUtils.convertUTCSecondstoLocalDatetime(int(res[0]))
    conn.close()
    return last_date.strftime("%Y.%m.%d")

def getDailyAverageConsumption(typ):
    conn = createConn()
    begindate = "20150323"
    beginvalue = 0.0
    sql = "SELECT datum, value FROM {0}_verbrauch ORDER BY datum DESC LIMIT 1;".format(typ)
    cur = conn.execute(sql)
    res = cur.fetchone()
    begininsec = dateUtils.createSecondsSince(begindate)
    conn.close()
    #return round(((res[1] - beginvalue) / ((res[0] - begininsec) / 86400)), 2)
    diffv = res[1] - beginvalue
    difftageinsec = res[0] - begininsec
    difftageintage = difftageinsec / 86400

    if typ == "wasser":
	    diffv *= 1000

    return diffv / difftageintage

#print getDailyAverageConsumption("strom")
#print getDailyAverageConsumption("wasser")
def createConn():
    os.chdir(str(os.path.dirname(os.path.abspath(__file__))))
    config = configparser.ConfigParser()
    config.read('config.ini')
    config.get('main', 'PATH_GRAPH_OUTPUT')
    return sqlite3.connect(config.get('main', 'DB_PATH_NAME_VERBRAUCH'))

def createConsumptionLastMonth(typ):
    actdt = dateUtils.createDateTimeToday().replace(day=1) - dt.timedelta(days=1)
    monthasint = int(actdt.strftime("%m"))
    yearasint = int(actdt.strftime("%Y"))
    return createConsumptionForMonth(typ, yearasint, monthasint)

def createConsumptionActualMonth(typ):
    actdt = dateUtils.createDateTimeToday()
    monthasint = int(actdt.strftime("%m"))
    yearasint = int(actdt.strftime("%Y"))
    return createConsumptionForMonth(typ, yearasint, monthasint)

def createConsumptionForMonth(typ, yearasint, monthasint):
    return createConsumptionForMonthSWITCH(typ, yearasint, monthasint, 'avg')

def createConsumptionForMonthABS(typ, yearasint, monthasint):
    return createConsumptionForMonthSWITCH(typ, yearasint, monthasint, 'abs')

def createConsumptionForMonthSWITCH(typ, yearasint, monthasint, switcher):
    #print typ, yearasint, monthasint
    begindate = str(yearasint) + "{0:02d}".format(monthasint) + '01'
    begindateforinterploation = dateUtils.createSecondsSince(begindate)
    interpolated_begin = getInterpolatedValueFordate(typ, begindateforinterploation)
    if interpolated_begin[1] == 0:
        return 0
    #print interpolated_begin
    enddate = str(yearasint) + "{0:02d}".format(monthasint) + str(calendar.monthrange(yearasint,monthasint)[1])
    #print enddate
    enddateforinterploation = dateUtils.createSecondsSince(enddate) + 86400 #add 1day(in seconds) to get 1 month
    interpolated_end = getInterpolatedValueFordate(typ, enddateforinterploation, True)
    if interpolated_end[1] == 0:
        return 0
    #print interpolated_end
    diffindays = (interpolated_end[0] - interpolated_begin[0]) / 86400
    if diffindays == 0:
	    diffindays = 1
    #print diffindays

    value = interpolated_end[1] - interpolated_begin[1]
    if typ == "wasser":
	    value *= 1000 # sho in liter

    if switcher == 'avg':
        return value / diffindays

    return value

def getInterpolatedValueFordate(typ, date, switcher=False):
    conn = createConn()
    sql = "SELECT datum, value FROM {0}_verbrauch WHERE datum < {1} ORDER BY datum DESC LIMIT 1;".format(typ, date)
    cur = conn.execute(sql)
    resPointBefore = cur.fetchone()
    if resPointBefore == None:
        conn.close()
        return (date, 0)    
    #print resPointBefore
    sql = "SELECT datum, value FROM {0}_verbrauch WHERE datum > {1} ORDER BY datum ASC LIMIT 1;".format(typ, date)
    cur = conn.execute(sql)
    resPointAfter = cur.fetchone()
    if resPointAfter == None:
        conn.close()
        if switcher:
            return (resPointBefore[0], resPointBefore[1])
        return (date, 0)
    #print resPointAfter
    slope = (resPointAfter[1]-resPointBefore[1])/(resPointAfter[0]-resPointBefore[0])
    interpolatedval = slope * (date - resPointBefore[0]) + resPointBefore[1]
    conn.close()
    return (date, interpolatedval)

#print createConsumptionActualMonth("strom")
#print createConsumptionLastMonth("strom")
#print createConsumptionActualMonth("wasser")
#print createConsumptionForMonth("strom", 2016, 1)
#print createConsumptionForMonth("strom", 2015, 12)
#print createConsumptionForMonth("wasser", 2016, 7)
#print createConsumptionForMonth("wasser", 2016, 8)
#print createConsumptionForMonth("wasser", 2016, 9)
#print createConsumptionForMonth("wasser", 2016, 10)

#print createConsumptionForMonthABS("strom", 2016, 1)
#print createConsumptionForMonthABS("strom", 2016, 2)
#print createConsumptionForMonthABS("strom", 2016, 3)
#print createConsumptionForMonthABS("strom", 2016, 4)
#print createConsumptionForMonthABS("strom", 2016, 5)
#print createConsumptionForMonthABS("strom", 2016, 6)
#print createConsumptionForMonthABS("strom", 2016, 7)
#print createConsumptionForMonthABS("strom", 2016, 8)
#print createConsumptionForMonthABS("strom", 2016, 9)
#print createConsumptionForMonthABS("wasser", 2016, 9)
#print(get_latest_insert_date("strom"))
#print(get_latest_insert_date("wasser"))